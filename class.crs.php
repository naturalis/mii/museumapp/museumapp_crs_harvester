<?php


class Crs extends BaseClass
{
    const CHUNK_SIZE = 100;
    private $masterlist = [];

    public function __destruct()
    {
        $this->db->close();
    }

    public function setDatabaseCredentials($p)
    {
        $this->db_credentials = $p;
    }

    public function connectDatabase()
    {
        $this->db = new mysqli(
            $this->db_credentials["host"],
            $this->db_credentials["user"],
            $this->db_credentials["pass"],
            $this->db_credentials["database"]
        );

        if ($this->db->connect_error) {
            $this->log(sprintf('could not connect: %s', $this->db->connect_error), 1, "CRS harvest");
            die();
        }

        $this->db->set_charset("utf8");
    }

    public function setMasterlist($list, $keyCol)
    {
        $this->masterlist = array_column($list, $keyCol);
    }

    public function setMediaUrls()
    {
        $this->mediaUrls = [];
        $arrs = array_chunk($this->masterlist, self::CHUNK_SIZE);

        foreach ($arrs as $arr) {
            $sql = $this->_getCrsQuery();
            // Replaced the 100x Oracle bind with a simple imploded string
            $res = $this->db->query(sprintf($sql, implode("','", $arr)));
            // Added the replace in the run script here
            while ($row = $res->fetch_object()) {
                if (!empty($row->registrationnumber) && !empty($row->url)) {
                    $this->mediaUrls[$row->registrationnumber][] = str_replace('+', '%20', $row->url);
                }
            }
        }
    }

    private function _getCrsQuery()
    {
        return "
            select s.registrationnumber, dbv.URL as url 
                from NCRS_SPECIMEN s
            inner join 
                NCRS_DATAGROUP dg ON dg.SPECIMENID = s.ID
            inner join 
                FRM_XMLBESCHRIJVINGEN x on s.XMLBESCHRIJVINGID = x.ID
            left join 
                frm_containerrelaties cr on x.containerid = cr.containedbyid
            left join 
                frm_xmlbeschrijvingen xmm on cr.containerid = xmm.containerid
            left join 
                FRM_DIGITALEBESTANDEN db on xmm.id = db.xmlbeschrijvingid
            left join 
                frm_digitaalbestand_versies dbv on dbv.digitaalbestandid = db.id and dbv.ENDDATE is null
            left join 
                NCRS_DETERMINATION det on dg.id = det.DATAGROUP
            where 
                s.registrationnumber IN ('%s') and det.preferred = 1
            order by 
                s.INST_COLL_SUBCOLL, s.registrationnumber ASC";
    }
}
