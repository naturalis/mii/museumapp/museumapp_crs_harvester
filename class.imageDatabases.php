<?php


class ImageDatabases extends BaseClass
{

    const TABLE_SELECTOR_MEDIALIB_URLS = 'medialib_urls';
    const TABLE_SELECTOR_SKIPPED_UNITIDS = 'skipped_unitids';
    const TABLE_SQUARES_MEDIALIB_URLS = 'medialib_urls_new';
    const TABLE_SQUARES_SKIPPED_NAMES = 'skipped_scientific_names_new';
    const TABLE_MASTER = 'tentoonstelling';
    public $SQLitePath = ["selector" => null, "squares" => null];

    public function initialize()
    {
        $this->_initializeSQLite();
        $this->_initializeMySQL();
    }

    private function _initializeSQLite()
    {
        $this->selector_db = new SQLite3($this->SQLitePath["selector"], SQLITE3_OPEN_READWRITE);
        $this->squares_db = new SQLite3($this->SQLitePath["squares"], SQLITE3_OPEN_READWRITE);
    }

    private function _initializeMySQL()
    {
        if (!isset($this->mysql_db_credentials)) {
            return;
        }

        $this->mysql_db = new mysqli(
            $this->mysql_db_credentials["host"],
            $this->mysql_db_credentials["user"],
            $this->mysql_db_credentials["pass"]
        );

        $this->mysql_db->select_db($this->mysql_db_credentials["database"]);
        $this->mysql_db->set_charset("utf8");
    }

    public function setDatabaseCredentials($p)
    {
        $this->mysql_db_credentials = $p;
    }

    public function setSQLitePath($source, $path)
    {
        if (array_key_exists($source, $this->SQLitePath)) {
            if (file_exists($path)) {
                $this->SQLitePath[$source] = $path;
            } else {
                throw new Exception(sprintf("SQLite path doesn't exist: %s", $path), 1);
            }
        } else {
            throw new Exception(sprintf("unknown SQLite source: %s", $source), 1);
        }
    }

    public function setMediaUrls($list)
    {
        $this->mediaUrls = $list;
    }

    public function selector_saveUnitIDAndUrls()
    {
        $this->inserted = 0;
        $this->existing = 0;
        $this->reset = 0;

        $this->setDbTransaction($this->selector_db);

        foreach ($this->mediaUrls as $unitid => $urls) {
            foreach ($urls as $url) {
                $url = str_ireplace("http://", "https://", $url);
                try {
                    $this->selector_saveUnitIDAndUrl($unitid, $url);
                } catch (Exception $e) {
                    $this->log($e->getMessage(), 3, "selector");
                }
            }
        }

        $this->setDbTransaction($this->selector_db, true);

        $this->log(sprintf("inserted %s media URLs", $this->inserted), 3, "selector");
        $this->log(sprintf("reset %s skipped unitids", $this->reset), 3, "selector");
        $this->log(sprintf("skipped %s existing media URLs", $this->existing), 3, "selector");
    }

    public function setDbTransaction($db, $commit = false)
    {
        $db->exec($commit ? 'COMMIT;' : 'BEGIN;');
    }

    public function selector_saveUnitIDAndUrl($unitid, $url)
    {
        $unitid = trim($unitid);
        $url = trim($url);

        if (strlen($unitid) == 0) {
            throw new Exception("empty unit_id");
        }

        if (strlen($url) == 0) {
            throw new Exception("empty URL");
        }

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new Exception("not a valid URL: $url");
        }

        $sql = $this->selector_db->prepare(
            sprintf('select count(*) as total from %s where unitid = ? and url = ?', self::TABLE_SELECTOR_MEDIALIB_URLS)
        );
        $sql->bindValue(1, $unitid);
        $sql->bindValue(2, $url);
        $a = $sql->execute();
        if ($a->fetchArray()[0] > 0) {
            $this->existing++;
            return;
        }

        $sql = $this->selector_db->prepare(
            sprintf('insert into %s (unitid,url) values (?,?)', self::TABLE_SELECTOR_MEDIALIB_URLS)
        );
        $sql->bindValue(1, $unitid);
        $sql->bindValue(2, $url);
        $sql->execute();

        $this->inserted++;

        $sql = $this->selector_db->prepare(
            sprintf('select count(*) as total from %s where unitid = ?', self::TABLE_SELECTOR_SKIPPED_UNITIDS)
        );
        $sql->bindValue(1, $unitid);
        $a = $sql->execute();

        if ($a->fetchArray()[0] > 0) {
            $sql = $this->selector_db->prepare(
                sprintf('delete from %s where unitid = ?', self::TABLE_SELECTOR_SKIPPED_UNITIDS)
            );
            $sql->bindValue(1, $unitid);
            $sql->execute();

            $this->reset++;
        }
    }

    public function squares_saveUnitIDAndUrls()
    {
        $this->inserted = 0;
        $this->existing = 0;
        $this->reset = 0;

        $this->setDbTransaction($this->squares_db);

        foreach ($this->mediaUrls as $unitid => $urls) {
            foreach ($urls as $url) {
                $url = str_ireplace("http://", "https://", $url);
                try {
                    $this->squares_saveUnitIDAndUrl($unitid, $url);
                } catch (Exception $e) {
                    $this->log($e->getMessage(), 3, "squares");
                }
            }
        }

        $this->setDbTransaction($this->squares_db, true);

        $this->log(sprintf("inserted %s media URLs", $this->inserted), 3, "squares");
        $this->log(sprintf("reset %s skipped names", $this->reset), 3, "squares");
        $this->log(sprintf("skipped %s existing media URLs", $this->existing), 3, "squares");
    }

    public function squares_saveUnitIDAndUrl($unitid, $url)
    {
        $unitid = trim($unitid);
        $url = trim($url);
        $url = str_ireplace("http://", "https://", $url);

        if (strlen($unitid) == 0) {
            throw new Exception("empty unit_id");
        }

        if (strlen($url) == 0) {
            throw new Exception("empty URL");
        }

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new Exception("not a valid URL: $url");
        }

        $parse = parse_url($url);

        if ($parse["host"] != "medialib.naturalis.nl") {
            throw new Exception("not a medialib URL: $url");
        }

        $ref = $this->_getMasterlistRef($unitid);

        if (empty($ref["unitid"])) {
            throw new Exception("can't find unitid in masterlist: $unitid");
        }

        if (empty($ref["name"])) {
            throw new Exception("can't find a name for unitid in masterlist: $unitid");
        }

        $name = $ref["name"];

        $sql = $this->squares_db->prepare(
            sprintf('select count(*) as total from %s where url = ?', self::TABLE_SQUARES_MEDIALIB_URLS)
        );
        $sql->bindValue(1, $url);
        $a = $sql->execute();
        if ($a->fetchArray()[0] > 0) {
            $this->existing++;
            return;
        }

        $sql = $this->squares_db->prepare(
            sprintf('insert into %s (unitid,scientific_name,url) values (?,?,?)', self::TABLE_SQUARES_MEDIALIB_URLS)
        );
        $sql->bindValue(1, $unitid);
        $sql->bindValue(2, $name);
        $sql->bindValue(3, $url);
        $sql->execute();

        $this->inserted++;

        $sql = $this->squares_db->prepare(
            sprintf('select count(*) as total from %s where scientific_name = ?', self::TABLE_SQUARES_SKIPPED_NAMES)
        );
        $sql->bindValue(1, $name);
        $a = $sql->execute();

        if ($a->fetchArray()[0] > 0) {
            $sql = $this->squares_db->prepare(
                sprintf('delete from %s where scientific_name = ?', self::TABLE_SQUARES_SKIPPED_NAMES)
            );
            $sql->bindValue(1, $name);
            $sql->execute();
            $this->reset++;
        }
    }

    private function _getMasterlistRef($unitid)
    {
        $sql = $this->mysql_db->query(
            "select Registratienummer as unitid, `SCname controle` as name 
                from " . self::TABLE_MASTER . " where Registratienummer = '" . $this->mysql_db->real_escape_string(
                $unitid
            ) . "'"
        );
        $row = $sql->fetch_assoc();
        return $row;
    }
}
