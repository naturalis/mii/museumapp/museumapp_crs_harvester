<?php

$pipeline_db["host"] = $_ENV["MYSQL_HOST"] ?? null;
$pipeline_db["user"] = $_ENV["MYSQL_USER"] ?? null;
$pipeline_db["pass"] = $_ENV["MYSQL_PASSWORD"] ?? null;
$pipeline_db["database"] = $_ENV["MYSQL_DATABASE"] ?? null;

$crs_db["host"] = $_ENV["CRS_HOST"] ?? null;
$crs_db["user"] = $_ENV["CRS_USER"] ?? null;
$crs_db["pass"] = $_ENV["CRS_PASSWORD"] ?? null;
$crs_db["database"] = $_ENV["CRS_DATABASE"] ?? null;

$selector_db = $_ENV["IMAGE_SELECTOR_DB_PATH"] ?? null;
$squares_db = $_ENV["IMAGE_SQUARES_DB_PATH"] ?? null;

try {
    include_once('class.baseClass.php');
    include_once('class.localDatabase.php');
    include_once('class.crs.php');
    include_once('class.imageDatabases.php');

    $d = new LocalDatabase;

    $d->setDatabaseCredentials($pipeline_db);
    $d->connectDatabase();
    $d->setMasterlist();

    $list = $d->getMasterlist();

    $c = new Crs;

    $c->setDatabaseCredentials($crs_db);
    $c->connectDatabase();
    $c->setMasterlist($list, "Registratienummer");
    $c->setMediaUrls();

    $urls = $c->getMediaUrls();

    $d->setMediaUrls($urls);
    $d->saveMediaUrls();

    $s = new ImageDatabases;

    $s->setDatabaseCredentials($pipeline_db);
    $s->setSQLitePath("selector", $selector_db);
    $s->setSQLitePath("squares", $squares_db);
    $s->initialize();
    $s->setMediaUrls($urls);
    $s->selector_saveUnitIDAndUrls();
    $s->squares_saveUnitIDAndUrls();

    echo "done\n";
} catch (Exception $e) {
    echo $e->getMessage(), "\n";
}
