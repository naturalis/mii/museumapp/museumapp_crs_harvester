<?php


class LocalDatabase extends BaseClass
{
    const TABLE_MASTER = 'tentoonstelling';
    const TABLE_CRS = 'crs';
    private $masterlist = [];

    public function setDatabaseCredentials($p)
    {
        $this->db_credentials = $p;
    }

    public function connectDatabase()
    {
        $this->db = new mysqli(
            $this->db_credentials["host"], $this->db_credentials["user"], $this->db_credentials["pass"]
        );

        $this->db->select_db($this->db_credentials["database"]);
        $this->db->set_charset("utf8");
    }

    public function getMasterlist()
    {
        return $this->masterlist;
    }

    public function setMasterlist()
    {
        $this->masterlist = [];

        $sql = $this->db->query("select * from " . self::TABLE_MASTER);
        while ($row = $sql->fetch_assoc()) {
            $this->masterlist[] = $row;
        }
    }

    public function setUnitidUrls()
    {
        return $this->masterlist;
    }

    public function setMediaUrls($list)
    {
        $this->mediaUrls = $list;
    }

    public function saveMediaUrls()
    {
        $this->db->query("truncate " . self::TABLE_CRS);

        $this->inserted = 0;

        $stmt = $this->db->prepare("insert into " . self::TABLE_CRS . " (REGISTRATIONNUMBER,URL) values (?,?)");
        $this->log(sprintf("truncated %s", self::TABLE_CRS), 3, "crs");

        foreach ($this->mediaUrls as $unitid => $urls) {
            foreach ($urls as $url) {
                $stmt->bind_param('ss', $unitid, $url);

                $stmt->execute();
                $this->inserted++;
            }

            $this->log(sprintf("inserted %s media URLs for '%s'", count($urls), $unitid), 3, "crs");
        }

        $this->log(sprintf("inserted %s media URLs", $this->inserted), 3, "crs");
    }
}
