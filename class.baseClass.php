<?php


class BaseClass
{
    public $db_credentials;
    public $db;
    public $mediaUrls = [];
    private $messages = [];

    public function getMediaUrls()
    {
        return $this->mediaUrls;
    }

    public function log($message, $level, $source = null)
    {
        $val = ["timestamp" => date('d-M-Y H:i:s'), "message" => $message, "level" => $level, "source" => $source];
        echo $val["timestamp"], " - ", $val["source"], " - ", $val["message"], "\n";
        $this->messages[] = $val;
    }

    public function getMessages()
    {
        return $this->messages;
    }

}